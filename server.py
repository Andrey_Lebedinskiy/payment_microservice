import hashlib
from concurrent import futures
from urllib import parse

import grpc
from payment_microservice import payment_pb2_grpc, payment_pb2


class PaymentService(payment_pb2_grpc.PaymentServiceServicer):
    def GeneratePaymentLink(self, request, context):
        merchant_login = request.merchant_login
        merchant_password_1 = request.merchant_password_1
        cost = request.cost
        number = request.number
        description = request.description
        is_test = request.is_test
        robokassa_payment_url = request.robokassa_payment_url

        # Ваш логический код для генерации платежной ссылки
        signature = self.calculate_signature(
            merchant_login,
            cost,
            number,
            merchant_password_1
        )

        data = {
            'MerchantLogin': merchant_login,
            'OutSum': cost,
            'InvId': number,
            'Description': description,
            'SignatureValue': signature,
            'IsTest': is_test
        }

        payment_link = f'{robokassa_payment_url}?{parse.urlencode(data)}'
        response = payment_pb2.GeneratePaymentLinkResponse(
            payment_link=payment_link
        )
        return response

    def calculate_signature(*args) -> str:
        """Create signature MD5.
        """
        return hashlib.md5(':'.join(str(arg) for arg in args).encode()).hexdigest()

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=2))

    payment_pb2_grpc.add_PaymentServiceServicer_to_server(
        PaymentService(), server
    )

    server.add_insecure_port('[::]:50051')
    server.start()
    print("Server started. Listening on port 50051.")
    server.wait_for_termination()

if __name__ == '__main__':
    serve()